﻿# Instalador Popped
Este aplicativo foi desenvolvido em C# (Windows Forms) e tem a finalidade de configurar os ambientes necessários para operação do aplicativo Popped para restaurantes.

# Funções:

  - Obtem os projetos da API e Dashboard através de um repositorio do GIT
  - Instala um servidor de PHP, Apache e MySQL (WAMP) em ambientes Windows 64 e 32bits
  - Cria um banco de dados para o app
  - Fixa o IP da máquina na rede

# Requisitos para execução e desenvolvimento do projeto 
   - Visual Studio 2008 ou superior
   - Framework .NET
   - Conexão com a Internet
   - InnoSetup (incluso no projeto)
   - IdpSetup (incluso no projeto)

# Requisitos para execução do aplicativo
  - Conexão com a Internet
  - Repositório GIT válido
 
# Utilização

  - Com o Visual Studio aberto, selecione a Opção 'Arquivo' > 'Abrir' e procure pelo arquivo `Popped.sln` no projeto
  - No arquivo `App.config` configure os dados do GIT (URL, usuário e senha e um branch a ser utilizado)
  - Execute o projeto na opção 'Iniciar' na parte superior do Visual Studio

# Gerar um novo instalador

  - Abra a pasta 'Config' e instale os dois arquivos (InnoSetup e IdpSetup).
  - Abra o arquivo poppedIss (localizado na mesma pasta 'Config' do projeto) utilizando o InnoSetup
  - Realize as alterações se necessário no arquivo 'poppedIss' (alteração de nome, versão, icone, adição de arquivos e etc)
  - Execute o script no InnoSetup (uma pasta Output será criada na pasta Config com o instalador do aplicativo)

# Arquivos de terceiros necessários para funcionamento do projeto:
  - Pasta GIT (contendo todos os arquivos necessários para executar comandos git)
  - composer.phar (atualizar bibliotecas através do composer)
  - popped_local.sql (arquivo .sql com a criação do banco de dados)
  - wamp (servidor Apache, PHP e MySQL)

