CREATE SCHEMA `popped_local`;
USE `popped_local`;

-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.28-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela local.additional
CREATE TABLE IF NOT EXISTS `additional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT 'Nome do adicional',
  `description` varchar(255) DEFAULT '' COMMENT 'Descrição do adicional',
  `price` decimal(15,2) NOT NULL COMMENT 'Preço do adicional',
  `image` varchar(255) DEFAULT NULL COMMENT 'Imagem do adicional',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status do adicional (ativo/inativo)',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do adicional',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração do adicional',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de remoção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_a_company_idx` (`id_company`),
  CONSTRAINT `fk_a_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.additional: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `additional` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional` ENABLE KEYS */;

-- Copiando estrutura para tabela local.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `photo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` enum('YES','NO') COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.admin: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Copiando estrutura para tabela local.admin_log
CREATE TABLE IF NOT EXISTS `admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL,
  `id_company` int(11) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_bin NOT NULL,
  `page` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_admin_log` (`id_admin`),
  KEY `fk_id_company_admin_log` (`id_company`),
  CONSTRAINT `fk_id_admin_log` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`),
  CONSTRAINT `fk_id_company_admin_log` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.admin_log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_log` ENABLE KEYS */;

-- Copiando estrutura para tabela local.admin_token
CREATE TABLE IF NOT EXISTS `admin_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL DEFAULT '0',
  `token` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_admin_token_admin` (`id_admin`),
  CONSTRAINT `fk_admin_token_admin` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.admin_token: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `admin_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_token` ENABLE KEYS */;

-- Copiando estrutura para tabela local.avaliation
CREATE TABLE IF NOT EXISTS `avaliation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `food` int(11) NOT NULL DEFAULT '5',
  `drink` int(11) NOT NULL DEFAULT '5',
  `music` int(11) NOT NULL DEFAULT '5',
  `environment` int(11) NOT NULL DEFAULT '5',
  `attendance` int(11) NOT NULL DEFAULT '5',
  `name` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_company` (`id_company`),
  CONSTRAINT `avaliation_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.avaliation: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `avaliation` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliation` ENABLE KEYS */;

-- Copiando estrutura para tabela local.cache
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela local.cache: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;

-- Copiando estrutura para tabela local.calendar
CREATE TABLE IF NOT EXISTS `calendar` (
  `date` date NOT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.calendar: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;

-- Copiando estrutura para tabela local.campaign
CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `active` enum('yes','no') COLLATE utf8mb4_bin NOT NULL DEFAULT 'yes',
  `order` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `sync` enum('yes','no','never') COLLATE utf8mb4_bin NOT NULL DEFAULT 'never',
  `finish_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Copiando dados para a tabela local.campaign: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;

-- Copiando estrutura para tabela local.campaign_company
CREATE TABLE IF NOT EXISTS `campaign_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_campaign` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `click_campaign` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remuneration` enum('bonus','paid') COLLATE utf8mb4_bin DEFAULT 'paid',
  `active` enum('yes','no') COLLATE utf8mb4_bin NOT NULL DEFAULT 'yes',
  `sync` enum('yes','no','never') COLLATE utf8mb4_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_campaign_company` (`id_campaign`),
  KEY `fk_restaurant_company` (`id_company`),
  CONSTRAINT `fk_campaign_company` FOREIGN KEY (`id_campaign`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_restaurant_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Copiando dados para a tabela local.campaign_company: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `campaign_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_company` ENABLE KEYS */;

-- Copiando estrutura para tabela local.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Nome da categoria',
  `image` varchar(255) DEFAULT NULL COMMENT 'Imagem da categoria',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status da categoria (ativo/inativo)',
  `order` int(11) NOT NULL COMMENT 'Ordem de exibição da categoria',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da categoria',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração da categoria',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_c_company_idx` (`id_company`),
  CONSTRAINT `fk_c_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.category: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Copiando estrutura para tabela local.category_exibition
CREATE TABLE IF NOT EXISTS `category_exibition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_category` int(11) NOT NULL COMMENT 'Id da categoria',
  `day` enum('SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY') NOT NULL COMMENT 'Dia de exibição da categoria',
  `at` time DEFAULT NULL COMMENT 'Range de horário ‘de’',
  `to` time DEFAULT NULL COMMENT 'Range de horário ‘até’',
  `display` enum('yes','no') DEFAULT 'yes' COMMENT 'Exibir categoria nessa data',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre categoria e exibição',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre categoria e exibição',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_ce_category_idx` (`id_category`),
  KEY `fk_ce_company_idx` (`id_company`),
  CONSTRAINT `fk_ce_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ce_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.category_exibition: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `category_exibition` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_exibition` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo
CREATE TABLE IF NOT EXISTS `combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(15,0) DEFAULT NULL,
  `active` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `all_you_can_eat` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_combo_company` (`id_company`),
  CONSTRAINT `FK_combo_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.combo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo_exibition
CREATE TABLE IF NOT EXISTS `combo_exibition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `combo_id` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `display` enum('yes','no') NOT NULL DEFAULT 'yes',
  `day` enum('SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY') DEFAULT NULL,
  `at` time DEFAULT NULL,
  `to` time DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__combo_id_exibition` (`combo_id`),
  KEY `FK_combo_exibition_company` (`company_id`),
  CONSTRAINT `FK__combo_id_exibition` FOREIGN KEY (`combo_id`) REFERENCES `combo` (`id`),
  CONSTRAINT `FK_combo_exibition_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=818 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.combo_exibition: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo_exibition` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo_exibition` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo_group
CREATE TABLE IF NOT EXISTS `combo_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `combo_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_combo_group_combo` (`combo_id`),
  KEY `FK_combo_group_company` (`company_id`),
  CONSTRAINT `FK_combo_group_combo` FOREIGN KEY (`combo_id`) REFERENCES `combo` (`id`),
  CONSTRAINT `FK_combo_group_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.combo_group: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo_group` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo_group_item
CREATE TABLE IF NOT EXISTS `combo_group_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `combo_group_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `additional_price` decimal(15,0) NOT NULL DEFAULT '0',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__product` (`product_id`),
  KEY `FK__combo_group` (`combo_group_id`),
  KEY `FK_combo_group_item_company` (`company_id`),
  CONSTRAINT `FK__combo_group` FOREIGN KEY (`combo_group_id`) REFERENCES `combo_group` (`id`),
  CONSTRAINT `FK__product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_combo_group_item_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.combo_group_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo_group_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo_group_item` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo_image
CREATE TABLE IF NOT EXISTS `combo_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_combo` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ci_combo` (`id_combo`),
  KEY `fk_ci_company` (`id_company`),
  CONSTRAINT `fk_ci_combo` FOREIGN KEY (`id_combo`) REFERENCES `combo` (`id`),
  CONSTRAINT `fk_ci_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.combo_image: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo_image` ENABLE KEYS */;

-- Copiando estrutura para tabela local.combo_type
CREATE TABLE IF NOT EXISTS `combo_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `combo_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` decimal(15,2) NOT NULL,
  `sync` enum('yes','no','never') COLLATE utf8_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_combo_type_company` (`company_id`),
  KEY `FK_combo_type_combo` (`combo_id`),
  CONSTRAINT `FK_combo_type_combo` FOREIGN KEY (`combo_id`) REFERENCES `combo` (`id`),
  CONSTRAINT `FK_combo_type_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.combo_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combo_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `combo_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.command
CREATE TABLE IF NOT EXISTS `command` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `total_value` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'Valor total da comanda',
  `number` int(11) NOT NULL,
  `opening_at` timestamp NULL DEFAULT NULL COMMENT 'Data de abertura da comanda',
  `closing_at` timestamp NULL DEFAULT NULL COMMENT 'Data de fechamento da comanda',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da comanda',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de atualização da comanda',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `fk_co_company_idx` (`id_company`),
  CONSTRAINT `fk_co_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.command: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `command` DISABLE KEYS */;
/*!40000 ALTER TABLE `command` ENABLE KEYS */;

-- Copiando estrutura para tabela local.command_table
CREATE TABLE IF NOT EXISTS `command_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_table` int(11) NOT NULL COMMENT 'Id da mesa',
  `id_command` int(11) NOT NULL COMMENT 'Id da comanda',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre comanda e mesa',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre comanda e mesa',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_ct_table_idx` (`id_table`),
  KEY `fk_ct_command_idx` (`id_command`),
  KEY `fk_ct_company_idx` (`id_company`),
  CONSTRAINT `fk_ct_command` FOREIGN KEY (`id_command`) REFERENCES `command` (`id`),
  CONSTRAINT `fk_ct_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_ct_table` FOREIGN KEY (`id_table`) REFERENCES `table` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.command_table: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `command_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `command_table` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company_type` int(11) DEFAULT NULL,
  `id_target_audience` int(11) DEFAULT NULL,
  `cnpj` varchar(45) NOT NULL,
  `ie` varchar(45) DEFAULT NULL,
  `trading_name` varchar(255) NOT NULL,
  `fantasy_name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `plan` int(11) DEFAULT NULL,
  `license` int(11) DEFAULT NULL,
  `blocked` enum('YES','NO') DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_c_company_type_idx` (`id_company_type`),
  KEY `fk_c_target_audience_idx` (`id_target_audience`),
  KEY `fk_company_plan_type` (`plan`),
  CONSTRAINT `fk_c_company_type` FOREIGN KEY (`id_company_type`) REFERENCES `company_type` (`id`),
  CONSTRAINT `fk_c_target_audience` FOREIGN KEY (`id_target_audience`) REFERENCES `target_audience` (`id`),
  CONSTRAINT `fk_company_plan_type` FOREIGN KEY (`plan`) REFERENCES `company_plan_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.company: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`id`, `id_company_type`, `id_target_audience`, `cnpj`, `ie`, `trading_name`, `fantasy_name`, `email`, `password`, `zip_code`, `state`, `city`, `address`, `number`, `complement`, `status`, `plan`, `license`, `blocked`, `sync`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(25, NULL, NULL, '', NULL, 'teste', 'teste', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL, 'yes', '2018-06-28 22:13:16', '2018-06-28 22:24:49', NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_access_level
CREATE TABLE IF NOT EXISTS `company_access_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level` enum('ADMINISTRATOR','MANAGER','FINANCIAL','OPERATOR','WAITER') COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.company_access_level: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `company_access_level` DISABLE KEYS */;
INSERT INTO `company_access_level` (`id`, `access_level`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'ADMINISTRATOR', '2018-06-28 22:14:29', '2018-06-28 22:14:29', NULL),
	(2, 'MANAGER', '2018-06-28 22:14:29', '2018-06-28 22:14:29', NULL),
	(3, 'FINANCIAL', '2018-06-28 22:14:29', '2018-06-28 22:14:29', NULL),
	(4, 'OPERATOR', '2018-06-28 22:14:29', '2018-06-28 22:14:29', NULL),
	(5, 'WAITER', '2018-06-28 22:14:29', '2018-06-28 22:14:29', NULL);
/*!40000 ALTER TABLE `company_access_level` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_log
CREATE TABLE IF NOT EXISTS `company_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_company` int(11) NOT NULL DEFAULT '0',
  `page` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sync` enum('yes','no','never') COLLATE utf8_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_user_log` (`id_user`),
  KEY `fk_id_company_user_log` (`id_company`),
  CONSTRAINT `fk_id_company_user_log` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_id_user_log` FOREIGN KEY (`id_user`) REFERENCES `company_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=577 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.company_log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_log` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_plan_type
CREATE TABLE IF NOT EXISTS `company_plan_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('START','PREMIUM') COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.company_plan_type: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `company_plan_type` DISABLE KEYS */;
INSERT INTO `company_plan_type` (`id`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'START', '2018-06-28 22:17:50', '2018-06-28 22:17:50', NULL),
	(2, 'PREMIUM', '2018-06-28 22:17:50', '2018-06-28 22:17:50', NULL);
/*!40000 ALTER TABLE `company_plan_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_type
CREATE TABLE IF NOT EXISTS `company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.company_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_user
CREATE TABLE IF NOT EXISTS `company_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `qr_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `active` enum('yes','no') CHARACTER SET utf8 NOT NULL DEFAULT 'yes',
  `access_level` int(11) DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_id_company_user` (`id_company`),
  KEY `FK_company_access_level` (`access_level`),
  CONSTRAINT `FK_company_access_level` FOREIGN KEY (`access_level`) REFERENCES `company_access_level` (`id`),
  CONSTRAINT `FK_id_company_user` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin7;

-- Copiando dados para a tabela local.company_user: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `company_user` DISABLE KEYS */;
INSERT INTO `company_user` (`id`, `id_company`, `name`, `photo`, `user`, `password`, `qr_password`, `active`, `access_level`, `sync`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(25, 25, 'teste', NULL, 'teste@gmail.com', '$2y$10$9m5YfyF.twihM9oWWN0ipuEoUmV9Mzc/A4VTxENs5PPCT06wLrV7.', '', 'yes', 1, 'yes', '2018-06-28 22:25:18', '2018-06-28 22:30:22', NULL);
/*!40000 ALTER TABLE `company_user` ENABLE KEYS */;

-- Copiando estrutura para tabela local.company_user_token
CREATE TABLE IF NOT EXISTS `company_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_company_user_token` (`id_user`),
  CONSTRAINT `fk_company_user_token` FOREIGN KEY (`id_user`) REFERENCES `company_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.company_user_token: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_user_token` ENABLE KEYS */;

-- Copiando estrutura para tabela local.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Nome da configuração',
  `description` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Descrição da configuração',
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'Imagem de logo',
  `color` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Cor em hexadecimal',
  `all_you_can_eat` enum('YES','NO') COLLATE utf8_bin NOT NULL COMMENT 'Modo rodízio',
  `use_command` enum('YES','NO') COLLATE utf8_bin NOT NULL COMMENT 'Usar comanda ou não',
  `id_company` int(11) NOT NULL,
  `sync` enum('yes','no','never') COLLATE utf8_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data da criação da configuração',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração da configuração',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de exclusão da configuração',
  PRIMARY KEY (`id`),
  KEY `fk_con_company` (`id_company`),
  CONSTRAINT `fk_con_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.config: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `name`, `description`, `image`, `color`, `all_you_can_eat`, `use_command`, `id_company`, `sync`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(25, 'teste', '', NULL, '#ccc', 'YES', 'YES', 25, 'yes', '2018-06-28 22:25:34', '2018-06-28 22:36:53', NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Copiando estrutura para tabela local.menu_type
CREATE TABLE IF NOT EXISTS `menu_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_mt_company_idx` (`id_company`),
  CONSTRAINT `fk_mt_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.menu_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `is_read` enum('YES','NO') COLLATE utf8_bin DEFAULT 'NO',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_from` (`from`),
  KEY `fk_user_id_to` (`to`),
  CONSTRAINT `fk_user_id_from` FOREIGN KEY (`from`) REFERENCES `admin` (`id`),
  CONSTRAINT `fk_user_id_to` FOREIGN KEY (`to`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.messages: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Copiando estrutura para tabela local.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_table` int(11) NOT NULL COMMENT 'Mesa em que o pedido foi realizado',
  `id_command` int(11) NOT NULL COMMENT 'Comanda que o pedido foi realizado',
  `status` enum('received','production','done') NOT NULL DEFAULT 'received' COMMENT 'Status do pedido (recebido, em produção, entregue)',
  `use_service` int(11) NOT NULL DEFAULT '0' COMMENT 'Verifica se o pedido usará serviço ou não',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do pedido',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração do pedido',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_o_table_idx` (`id_table`),
  KEY `fk_o_command_idx` (`id_command`),
  KEY `fk_o_company_idx` (`id_company`),
  CONSTRAINT `fk_o_command` FOREIGN KEY (`id_command`) REFERENCES `command` (`id`),
  CONSTRAINT `fk_o_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_o_table` FOREIGN KEY (`id_table`) REFERENCES `table` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.order: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Copiando estrutura para tabela local.order_command
CREATE TABLE IF NOT EXISTS `order_command` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_table` int(11) NOT NULL COMMENT 'Id da mesa',
  `id_command` int(11) NOT NULL COMMENT 'Id da comanda',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre pedido e comanda',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre pedido e comanda',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_oc_table_idx` (`id_table`),
  KEY `fk_oc_command_idx` (`id_command`),
  KEY `fk_oc_company_idx` (`id_company`),
  KEY `fk_oc_order` (`id_order`),
  CONSTRAINT `fk_oc_command` FOREIGN KEY (`id_command`) REFERENCES `command` (`id`),
  CONSTRAINT `fk_oc_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_oc_order` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`),
  CONSTRAINT `fk_oc_table` FOREIGN KEY (`id_table`) REFERENCES `table` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.order_command: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_command` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_command` ENABLE KEYS */;

-- Copiando estrutura para tabela local.order_item
CREATE TABLE IF NOT EXISTS `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL COMMENT 'Id do pedido',
  `id_product` int(11) DEFAULT NULL COMMENT 'Id do produto',
  `id_product_type` int(11) DEFAULT NULL COMMENT 'Tipo do produto',
  `id_menu_type` int(11) DEFAULT NULL COMMENT 'Id do tipo de cardápio',
  `id_combo` int(11) DEFAULT NULL COMMENT 'id do combo',
  `id_combo_type` int(11) DEFAULT NULL,
  `obs` varchar(255) DEFAULT '' COMMENT 'Observação do produto (Ex: sem mostarda, sem alface etc…)',
  `price` decimal(10,2) DEFAULT NULL COMMENT 'Preço do item',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `is_combo` enum('yes','no') DEFAULT 'no' COMMENT 'Diferencia item do tipo combo',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do item no pedido',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração do item no pedido',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_oi_order_idx` (`id_order`),
  KEY `fk_oi_product_idx` (`id_product`),
  KEY `fk_oi_product_Type_idx` (`id_product_type`),
  KEY `fk_oi_menu_type_idx` (`id_menu_type`),
  KEY `fk_oi_company_idx` (`id_company`),
  KEY `fk_oi_combo` (`id_combo`),
  KEY `fk_oi_combo_Type` (`id_combo_type`),
  CONSTRAINT `fk_oi_combo` FOREIGN KEY (`id_combo`) REFERENCES `combo` (`id`),
  CONSTRAINT `fk_oi_combo_Type` FOREIGN KEY (`id_combo_type`) REFERENCES `combo_type` (`id`),
  CONSTRAINT `fk_oi_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oi_menu_type` FOREIGN KEY (`id_menu_type`) REFERENCES `menu_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oi_order` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oi_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oi_product_Type` FOREIGN KEY (`id_product_type`) REFERENCES `product_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.order_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;

-- Copiando estrutura para tabela local.order_item_additional
CREATE TABLE IF NOT EXISTS `order_item_additional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_order_item` int(11) NOT NULL COMMENT 'Id do item_pedido',
  `id_additional` int(11) NOT NULL COMMENT 'Id do adicional',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_oia_order_item_idx` (`id_order_item`),
  KEY `fk_oia_additional_idx` (`id_additional`),
  KEY `fk_oia_company_idx` (`id_company`),
  CONSTRAINT `fk_oia_additional` FOREIGN KEY (`id_additional`) REFERENCES `additional` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oia_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oia_order_item` FOREIGN KEY (`id_order_item`) REFERENCES `order_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.order_item_additional: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_item_additional` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_item_additional` ENABLE KEYS */;

-- Copiando estrutura para tabela local.order_item_preference
CREATE TABLE IF NOT EXISTS `order_item_preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_order_item` int(11) NOT NULL COMMENT 'Id do item_pedido',
  `id_preference` int(11) DEFAULT NULL,
  `id_preference_item` int(11) unsigned DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da preferencia para o item_pedido',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de alteração da preferencia para o item_pedido',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_oip_order_item_idx` (`id_order_item`),
  KEY `fk_oip_company_idx` (`id_company`),
  KEY `fk_oip_preference` (`id_preference`),
  KEY `fk_oip_preference_item` (`id_preference_item`),
  CONSTRAINT `fk_oip_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oip_order_item` FOREIGN KEY (`id_order_item`) REFERENCES `order_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oip_preference` FOREIGN KEY (`id_preference`) REFERENCES `preference` (`id`),
  CONSTRAINT `fk_oip_preference_item` FOREIGN KEY (`id_preference_item`) REFERENCES `preference_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.order_item_preference: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_item_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_item_preference` ENABLE KEYS */;

-- Copiando estrutura para tabela local.preference
CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display` varchar(45) DEFAULT 'yes',
  `multiple` enum('yes','no') DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `order` int(11) NOT NULL DEFAULT '0',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_preference_company` (`id_company`),
  CONSTRAINT `fk_preference_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.preference: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;

-- Copiando estrutura para tabela local.preference_category
CREATE TABLE IF NOT EXISTS `preference_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `id_category` int(11) NOT NULL COMMENT 'ID da categoria\n',
  `id_preference` int(11) NOT NULL COMMENT 'ID da preferencia\n',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração\n',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_preference_category_category_idx` (`id_category`),
  KEY `fk_preference_category_preference_idx` (`id_preference`),
  KEY `fk_preference_category_company` (`id_company`),
  CONSTRAINT `fk_preference_category_category_idx` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_preference_category_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_preference_category_preference_idx` FOREIGN KEY (`id_preference`) REFERENCES `preference` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.preference_category: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preference_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference_category` ENABLE KEYS */;

-- Copiando estrutura para tabela local.preference_item
CREATE TABLE IF NOT EXISTS `preference_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_preference` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_preference_item_company` (`id_company`),
  KEY `fk_preference_item_preference` (`id_preference`),
  CONSTRAINT `fk_preference_item_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_preference_item_preference` FOREIGN KEY (`id_preference`) REFERENCES `preference` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5408 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.preference_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preference_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference_item` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_system` int(11) DEFAULT NULL COMMENT 'Id do produto em um sistema externo (futuras integrações)',
  `name` varchar(100) NOT NULL COMMENT 'Nome do produto',
  `description` varchar(255) DEFAULT NULL COMMENT 'Descrição do produto',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status do produto (ativo/inativo)',
  `order` int(11) NOT NULL COMMENT 'Ordem de exibição do produto',
  `all_you_can_eat` enum('yes','no') NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do produto',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração do produto',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de exclusão do produto',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_p_company_idx` (`id_company`),
  CONSTRAINT `fk_p_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_additional
CREATE TABLE IF NOT EXISTS `product_additional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `id_product` int(11) NOT NULL COMMENT 'ID do produto\n',
  `id_additional` int(11) NOT NULL COMMENT 'ID do adicional\n',
  `order` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active' COMMENT 'Status do adicional para o produto\n',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração\n',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_product_additional_product_idx` (`id_product`),
  KEY `fk_product_additional_additional_idx` (`id_additional`),
  KEY `fk_product_additional_company` (`id_company`),
  CONSTRAINT `fk_product_additional_additional` FOREIGN KEY (`id_additional`) REFERENCES `additional` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_additional_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_product_additional_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_additional: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_additional` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_additional` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL COMMENT 'Id do produto',
  `id_category` int(11) NOT NULL COMMENT 'Id da categoria',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre produto e categoria',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre produto e categoria',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_pc_product_idx` (`id_product`),
  KEY `fk_pc_category_idx` (`id_category`),
  KEY `fk_pc_company_idx` (`id_company`),
  CONSTRAINT `fk_pc_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pc_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pc_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_category: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_exibition
CREATE TABLE IF NOT EXISTS `product_exibition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL COMMENT 'Id do produto',
  `day` enum('SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY') NOT NULL COMMENT 'Dia de exibição do produto',
  `at` time DEFAULT NULL COMMENT 'Range de horário ‘de’',
  `to` time DEFAULT NULL COMMENT 'Range de horário ‘até’',
  `display` enum('yes','no') DEFAULT 'yes' COMMENT 'Exibir produto nessa data',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre produto e exibição',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre produto e exibição',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_pe_product_idx` (`id_product`),
  KEY `fk_pe_company_idx` (`id_company`),
  CONSTRAINT `fk_pe_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pe_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1149 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_exibition: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_exibition` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_exibition` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_image
CREATE TABLE IF NOT EXISTS `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_pi_product_idx` (`id_product`),
  KEY `fk_pi_company_idx` (`id_company`),
  CONSTRAINT `fk_pi_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pi_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_image: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_preference
CREATE TABLE IF NOT EXISTS `product_preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `id_product` int(11) NOT NULL COMMENT 'ID do produto',
  `id_preference` int(11) NOT NULL COMMENT 'ID da preferencia',
  `status` enum('active','inactive') DEFAULT 'active' COMMENT 'Status da preferencia para o produto',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_product_preference_product_idx` (`id_product`),
  KEY `fk_product_preference_preference_idx` (`id_preference`),
  KEY `fk_product_preference_company` (`id_company`),
  CONSTRAINT `fk_product_preference_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_product_preference_preference` FOREIGN KEY (`id_preference`) REFERENCES `preference` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_preference_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_preference: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_preference` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_sale
CREATE TABLE IF NOT EXISTS `product_sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL COMMENT 'Id do produto',
  `id_sale_type` int(11) NOT NULL COMMENT 'Tipo da promoção',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status da promoção',
  `quantity` int(11) DEFAULT NULL COMMENT 'Quantidade da promoção',
  `day` int(11) DEFAULT NULL COMMENT 'Dia da promoção',
  `month` int(11) DEFAULT NULL COMMENT 'Ano da promoção',
  `year` int(11) DEFAULT NULL COMMENT 'Ano da promoção',
  `at` time DEFAULT NULL COMMENT 'Range de horário ‘de’',
  `to` time DEFAULT NULL COMMENT 'Range de horário ‘até’',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_ps_product_idx` (`id_product`),
  KEY `fk_ps_sale_type_idx` (`id_sale_type`),
  KEY `fk_ps_company_idx` (`id_company`),
  CONSTRAINT `fk_ps_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ps_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ps_sale_type` FOREIGN KEY (`id_sale_type`) REFERENCES `sale_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_sale: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sale` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_tag
CREATE TABLE IF NOT EXISTS `product_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL COMMENT 'Id do produto',
  `id_tag` int(11) NOT NULL COMMENT 'Id da restrição',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status da relação entre produto e restrição',
  `display` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da relação entre produto e restrição',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração na relação entre produto e restrição',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_pt_product_idx` (`id_product`),
  KEY `fk_pt_tag_idx` (`id_tag`),
  KEY `fk_ptr_company_idx` (`id_company`),
  CONSTRAINT `fk_ptr_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ptr_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ptr_tag` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_tag: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_tag` ENABLE KEYS */;

-- Copiando estrutura para tabela local.product_type
CREATE TABLE IF NOT EXISTS `product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL COMMENT 'Id do produto',
  `name` varchar(100) NOT NULL COMMENT 'Nome do tipo (Ex: X-Bacon simples = produto tipo 1, X-Bacon duplo = produto tipo 2)',
  `description` varchar(255) DEFAULT NULL COMMENT 'Descrição do tipo do produto',
  `price` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_pt_product_idx` (`id_product`),
  KEY `fk_pt_company_idx` (`id_company`),
  CONSTRAINT `fk_pt_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pt_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.product_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.restaurant_campaign
CREATE TABLE IF NOT EXISTS `restaurant_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `id_company` int(11) NOT NULL,
  `campaign_type` enum('product','url','institutional','satisfaction') COLLATE utf8mb4_bin NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `active` enum('yes','no') COLLATE utf8mb4_bin NOT NULL,
  `click_counter` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order` int(11) NOT NULL,
  `sync` enum('yes','no','never') COLLATE utf8mb4_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_campaign_restaurants_company` (`id_company`),
  CONSTRAINT `fk_campaign_restaurants_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Copiando dados para a tabela local.restaurant_campaign: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `restaurant_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_campaign` ENABLE KEYS */;

-- Copiando estrutura para tabela local.restaurant_campaign_product
CREATE TABLE IF NOT EXISTS `restaurant_campaign_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_campaign` int(11) NOT NULL,
  `sync` enum('yes','no','never') COLLATE utf8mb4_bin NOT NULL DEFAULT 'never',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_campaign` (`id_product`),
  KEY `fk_campaign_product_company` (`id_company`),
  KEY `fk_campaign_product_company_restaurant` (`id_campaign`),
  CONSTRAINT `fk_campaign_product_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_campaign_product_company_restaurant` FOREIGN KEY (`id_campaign`) REFERENCES `restaurant_campaign` (`id`),
  CONSTRAINT `fk_product_campaign` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Copiando dados para a tabela local.restaurant_campaign_product: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `restaurant_campaign_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_campaign_product` ENABLE KEYS */;

-- Copiando estrutura para tabela local.sale_type
CREATE TABLE IF NOT EXISTS `sale_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'Nome da promoção',
  `description` varchar(255) DEFAULT NULL COMMENT 'Descrição da promoção',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Status da promoção',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_st_company_idx` (`id_company`),
  CONSTRAINT `fk_st_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.sale_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sale_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_type` ENABLE KEYS */;

-- Copiando estrutura para tabela local.sincronize
CREATE TABLE IF NOT EXISTS `sincronize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sincronize_company` (`id_company`),
  CONSTRAINT `fk_sincronize_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Copiando dados para a tabela local.sincronize: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sincronize` DISABLE KEYS */;
/*!40000 ALTER TABLE `sincronize` ENABLE KEYS */;

-- Copiando estrutura para tabela local.table
CREATE TABLE IF NOT EXISTS `table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_waiter` int(11) DEFAULT NULL,
  `number` int(11) NOT NULL COMMENT 'número de cada mesa',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da mesa',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração da mesa',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_t_company_idx` (`id_company`),
  KEY `fk_id_waiter` (`id_waiter`),
  CONSTRAINT `fk_id_waiter` FOREIGN KEY (`id_waiter`) REFERENCES `company_user` (`id`),
  CONSTRAINT `fk_t_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.table: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `table` DISABLE KEYS */;
/*!40000 ALTER TABLE `table` ENABLE KEYS */;

-- Copiando estrutura para tabela local.tag
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL COMMENT 'Nome da tag (Ex: sem glúten, vegano etc…)',
  `status` enum('active','inactive') DEFAULT 'active' COMMENT 'Status da tag (ativo/inativo)',
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação da tag',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração da tag',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `image` varchar(255) DEFAULT NULL,
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_t_company_idx` (`id_company`),
  CONSTRAINT `fk_tag_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.tag: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;

-- Copiando estrutura para tabela local.target_audience
CREATE TABLE IF NOT EXISTS `target_audience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  `sync` enum('yes','no','never') NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_target_audience_company` (`id_company`),
  CONSTRAINT `fk_target_audience_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.target_audience: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `target_audience` DISABLE KEYS */;
/*!40000 ALTER TABLE `target_audience` ENABLE KEYS */;

-- Copiando estrutura para tabela local.technical_assistance
CREATE TABLE IF NOT EXISTS `technical_assistance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sync` enum('yes','no','never') COLLATE utf8_bin NOT NULL DEFAULT 'never',
  PRIMARY KEY (`id`),
  KEY `fk_assistance_user_from` (`from`),
  KEY `fk_assistance_user_to` (`to`),
  CONSTRAINT `fk_assistance_user_from` FOREIGN KEY (`from`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_assistance_user_to` FOREIGN KEY (`to`) REFERENCES `admin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Copiando dados para a tabela local.technical_assistance: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `technical_assistance` DISABLE KEYS */;
/*!40000 ALTER TABLE `technical_assistance` ENABLE KEYS */;

-- Copiando estrutura para tabela local.waiter_call
CREATE TABLE IF NOT EXISTS `waiter_call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_table` int(11) NOT NULL COMMENT 'Id da mesa que chamou o garçom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de alteração',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`id`),
  KEY `fk_wc_table_idx` (`id_table`),
  KEY `fk_wc_company_idx` (`id_company`),
  CONSTRAINT `fk_wc_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wc_table` FOREIGN KEY (`id_table`) REFERENCES `table` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela local.waiter_call: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `waiter_call` DISABLE KEYS */;
/*!40000 ALTER TABLE `waiter_call` ENABLE KEYS */;

-- Copiando estrutura para trigger local.AI_order_item_additional
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `AI_order_item_additional` AFTER INSERT ON `order_item_additional` FOR EACH ROW BEGIN
	DECLARE additionalPrice INT;
	
	SET additionalPrice = (SELECT price FROM additional WHERE id = NEW.id_additional);
	
	UPDATE order_item SET price = (price + additionalPrice) WHERE id = NEW.id_order_item;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Copiando estrutura para trigger local.AU_order_item
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `AU_order_item` AFTER UPDATE ON `order_item` FOR EACH ROW BEGIN
 DECLARE command_id int(11);
 
 set command_id = (select id_command from `order` as ord where ord.id = new.id_order);
   IF(NEW.deleted_at IS NULL ) THEN
   	
   	update command as co set co.total_value = (select sum(oi.price) from `order`as od inner join order_item as oi on oi.id_order = od.id where od.id_command = command_id) where co.id = command_id; 
   END IF;
end//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Copiando estrutura para trigger local.AU_order_service
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER';
DELIMITER //
CREATE TRIGGER `AU_order_service` AFTER UPDATE ON `order` FOR EACH ROW BEGIN
	declare quantity_item integer;
	if(new.use_service != old.use_service) then
			update order_item set quantity = quantity where new.id = id_order;
	end if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Copiando estrutura para trigger local.BI_order_item
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `BI_order_item` BEFORE INSERT ON `order_item` FOR EACH ROW BEGIN
declare price_item decimal(10,2);
declare value_item decimal(10,2);
declare command_id int(11);
set command_id = (select id_command from `order` as ord where ord.id = new.id_order);
IF(NEW.is_combo = 'yes') THEN 
SET price_item = IFNULL(
(SELECT price FROM combo_type WHERE combo_id = NEW.id_combo AND combo_type.id = NEW.id_combo_type)
 *NEW.quantity + 
 COALESCE((SELECT SUM(ad.price) FROM order_item_additional oia LEFT JOIN additional ad ON oia.id_additional = ad.id WHERE oia.id_order_item = NEW.id),0),0);
 if((select use_service from `order` where id = new.id_order) = 1) then
	set value_item = price_item * 0.1;
	set price_item = price_item + value_item;
 end if;
 
 set new.price = price_item;
 
ELSE
SET price_item = IFNULL(
(SELECT price FROM product_type WHERE id_product = NEW.id_product AND id = NEW.id_product_type )
 *NEW.quantity + 
 COALESCE((SELECT SUM(ad.price) FROM order_item_additional oia LEFT JOIN additional ad ON oia.id_additional = ad.id WHERE oia.id_order_item = NEW.id),0),0);
 if((select use_service from `order` where id = new.id_order) = 1) then
	set value_item = price_item * 0.1;
	set price_item = price_item + value_item;
 end if;
 
 set new.price = price_item;
 END IF;
 update command as co set total_value = price_item + (select sum(oi.price) from `order`as od inner join order_item as oi on oi.id_order = od.id where od.id_command = command_id) where co.id = command_id;
 end//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Copiando estrutura para trigger local.BU_order_item
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `BU_order_item` BEFORE UPDATE ON `order_item` FOR EACH ROW BEGIN
   DECLARE value_item decimal(10,2);
 DECLARE price_item decimal(10,2);
 IF (NEW.`is_combo` = 'yes') then
 set price_item = (select
    price
from
    combo_type
where
    combo_id = new.id_combo and combo_type.id = new.id_combo_type)
 * new.quantity + coalesce((select
       sum(ad.price)
   from
       order_item_additional oia
   left join additional ad on oia.id_additional = ad.id
   where
       oia.id_order_item = new.id), 0);
       
   	if ((select use_service from `order`  where id = new.id_order) = 1) then
       	set value_item = price_item * 0.1;
       	set price_item = price_item + value_item;
       end if;

       set NEW.price = price_item;

 
 
 else
set price_item = (select
    price
from
    product_type
where
    id_product = new.id_product
    and
    id = new.id_product_type) * new.quantity + coalesce((select
       sum(ad.price)
   from
       order_item_additional oia
   left join additional ad on oia.id_additional = ad.id
   where
       oia.id_order_item = new.id), 0);
       
   	if ((select use_service from `order`  where id = new.id_order) = 1) then
       	set value_item = price_item * 0.1;
       	set price_item = price_item + value_item;
       end if;
       
   
       
       set NEW.price = price_item;
   end if;
end//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
