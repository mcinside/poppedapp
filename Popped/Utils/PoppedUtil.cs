﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Management;
using MySql.Data.MySqlClient;
using System.Data;
using System.Net.NetworkInformation;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Popped.Utils
{
    class PoppedUtil
    {
        // Verificar se o software está instalado
        public static bool IsSoftwareInstalled(string softwareName)
        {
            var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall") ??
                      Registry.LocalMachine.OpenSubKey(
                          @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");

            if (key == null)
                return false;

            return key.GetSubKeyNames()
                .Select(keyName => key.OpenSubKey(keyName))
                .Select(subkey => subkey.GetValue("DisplayName") as string)
                .Any(displayName => displayName != null && displayName.Contains(softwareName));
        }

        // Obter o IP local da maquina
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        // Fixar o IP para a maquina na rede
        public static void setIP(string ip_address)
        {
            ManagementClass objMC =
              new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"] && (string)objMO["Description"] == GetActiveEthernetOrWifiNetworkInterface().Description)
                {

                    // Set DefaultGateway
                    var newGateway = objMO.GetMethodParameters("SetGateways");
                    Console.WriteLine(newGateway);
                    var defaultGateway = new string[] { GetGateway(ip_address)};
                    newGateway["DefaultIPGateway"] = defaultGateway;
                    newGateway["GatewayCostMetric"] = new int[] { 1 };

                    // Set IPAddress and Subnet Mask
                    var newAddress = objMO.GetMethodParameters("EnableStatic");
                    newAddress["IPAddress"] = new string[] { ip_address };
                    newAddress["SubnetMask"] = new string[] { "255.255.255.0" };

                    objMO.InvokeMethod("EnableStatic", newAddress, null);
                    objMO.InvokeMethod("SetGateways", newGateway, null);

                    string[] Dns = { "8.8.8.8", "8.8.4.4" };
                    ManagementBaseObject objdns = objMO.GetMethodParameters("SetDNSServerSearchOrder");
                    if (objdns != null)
                        {
                            objdns["DNSServerSearchOrder"] = Dns;
                            objMO.InvokeMethod("SetDNSServerSearchOrder", objdns, null);
                        }
                }
             }
        }

        public static string GetGateway(string ip)
        {
            string myIpString = GetDefaultGateway().ToString();
            Match match = Regex.Match(myIpString, @"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}");
            if (match.Success)
            {
                return myIpString;
            }
            else
            {
                return GetGatewayString(ip);
            }
        }

        public static IPAddress GetDefaultGateway()
        {
            return NetworkInterface
                .GetAllNetworkInterfaces()
                .Where(n => n.OperationalStatus == OperationalStatus.Up)
                .Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .SelectMany(n => n.GetIPProperties()?.GatewayAddresses)
                .Select(g => g?.Address)
                .Where(a => a != null)
                // .Where(a => a.AddressFamily == AddressFamily.InterNetwork)
                // .Where(a => Array.FindIndex(a.GetAddressBytes(), b => b != 0) >= 0)
                .FirstOrDefault();
        }

        public static NetworkInterface GetActiveEthernetOrWifiNetworkInterface()
        {
            var Nic = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(
                a => a.OperationalStatus == OperationalStatus.Up &&
                (a.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || a.NetworkInterfaceType == NetworkInterfaceType.Ethernet) &&
                a.GetIPProperties().GatewayAddresses.Any(g => g.Address.AddressFamily.ToString() == "InterNetwork"));

            return Nic;
        }

        public static void SetDNS(string DnsString, string AlternativeDns)
        {
            string[] Dns = { DnsString, AlternativeDns };
            
            while (GetActiveEthernetOrWifiNetworkInterface() == null)
            {
                SetDNS(DnsString, AlternativeDns);
            }
            var CurrentInterface = GetActiveEthernetOrWifiNetworkInterface();


            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();
            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].ToString().Contains(CurrentInterface.Description))
                    {
                        ManagementBaseObject objdns = objMO.GetMethodParameters("SetDNSServerSearchOrder");
                        if (objdns != null)
                        {
                            objdns["DNSServerSearchOrder"] = Dns;
                            objMO.InvokeMethod("SetDNSServerSearchOrder", objdns, null);
                        }
                    }
                }
            }
        }

        // Verificar se o banco de dados existe
        public static bool DBExists()
        {
            // Nome do banco de dados
            string dbName = "local_popped";

            string connString = "";

            connString = "Server=localhost;Uid=root;Pwd=;SslMode=none";
            
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();

            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                using (MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM information_schema.schemata WHERE SCHEMA_NAME='" + dbName + "'", connection))
                {

                    if (cmd.ExecuteNonQuery() > 0)
                    {
             
                        connection.Close();
                        return true;
                    }

                    connection.Close();
                    return false;
                }

            } catch (Exception ex)
            {
                if (ex.Message.Contains("denied"))
                {
                    // Conectar com senha
                    connString = "Server=localhost;Uid=root;Pwd=" + ConfigurationManager.AppSettings["db_password"] + ";SslMode=none";
                    connection = new MySqlConnection(connString);
                    command = connection.CreateCommand();

                    try
                    {
                        if (connection.State != ConnectionState.Open)
                        {
                            connection.Open();
                        }

                        using (MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM information_schema.schemata WHERE SCHEMA_NAME='" + dbName + "'", connection))
                        {

                            cmd.Parameters.AddWithValue("@dbName", dbName);
                            if (cmd.ExecuteNonQuery() != 0)
                            {

                                connection.Close();
                                return true;
                            }

                            connection.Close();
                            return false;
                        }

                    }
                    catch (Exception exc)
                    {
                       
                        Console.WriteLine("Exception DB Exists : " + exc.Message);
                        connection.Close();

                        return false;
                    }
                }

                Console.WriteLine("Exception DB Exists : " + ex.Message);
                connection.Close();

                return false;
            }
        }

        // Verificar se o script do banco existe
        public static bool ApiExists()
        {
            Console.WriteLine("API Exists : " + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            return Directory.Exists(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings["api_repository_folder"]);
        }

        // Método para obter o GateWay através de uma string de IP
        public static string GetGatewayString(string ipAddress)
        {
            string[] values = ipAddress.Split('.');

            return values[0] + "." + values[1] + "." + values[2] + "." + "1";
        }
    }
}
