﻿using System;
using System.Windows.Forms;
using Popped.Utils;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Popped
{
    public partial class Splash : Form
    {
        // Versão do WAMP
        public const string WAMP_NAME = "Wampserver32 3.1.3";
        // Versão do GIT
        public const string GIT_NAME = "Git version 2.18.0";

        public string outputString = "";
        public string lastOutputString = "";

        private System.Windows.Forms.Timer timerOutput;

        public Splash()
        {
            InitializeComponent();
            FormClosing += Splash_FormClosing;
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            reloadUI();
        }

        private void Splash_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Esconder campos
            btnInitialConfig.Visible = false;
            btnStartOperation.Visible = false;
            progressBar.Visible = false;
            gbIP.Visible = false;
            lblMessage.Visible = false;
            pbLogo.Visible = false;

            // Exibir mensagem de encerramento
            lblFinishing.Visible = true;

            Console.WriteLine("ENCERRANDO");
            ExecuteCommand("NET STOP wampapache");
            ExecuteCommand("NET STOP wampmysqld");
            ExecuteCommand("taskkill /IM php.exe /F");

            CleanProcess();
        }

        // Verificar se o script do banco existe
        private bool DbScriptExists()
        {
            return File.Exists(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\popped_local.sql");
        }

        // Recarregar os campos visuais
        private void reloadUI()
        {
            // Verificar se o WAMP está instalado
            if (PoppedUtil.IsSoftwareInstalled(WAMP_NAME))
            {
                // Verificar se o arquivo poppedl_local.sql existe no diretorio
                if (DbScriptExists())
                {
                    Invoke((MethodInvoker)delegate {
                        // Esconder botão de configuração inicial
                        btnInitialConfig.Visible = false;
                        btnStartOperation.Visible = true;
                        btnImportDb.Visible = false;
                        progressBar.Visible = false;
                        gbIP.Visible = false;
                        btnUpdateAPI.Visible = true;
                    });
                } else
                {
                    Invoke((MethodInvoker)delegate {
                        // Esconder botão de configuração inicial
                        btnInitialConfig.Visible = false;
                        btnStartOperation.Visible = false;
                        btnImportDb.Visible = true;
                        progressBar.Visible = false;
                        gbIP.Visible = false;
                        btnUpdateAPI.Visible = true;
                    });
                }
            }
            else
            {
                Invoke((MethodInvoker)delegate {
                    btnInitialConfig.Visible = true;
                    btnStartOperation.Visible = false;
                    progressBar.Visible = false;
                    gbIP.Visible = false;
                });
            }
        }
        
        // Clique do botão para iniciar a configuração
        private void btnInitialConfigClick(object sender, EventArgs e)
        {
            btnInitialConfig.Enabled = false;
            progressBar.Visible = true;

            // Abrir o WAMP
            Process p = new Process();
            p.Exited += new EventHandler(p_Exited);
            p.StartInfo.FileName = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\wamp.exe";
            p.EnableRaisingEvents = true;
            p.Start();

            // Aumentar o progress bar
            progressBar.Value = 30;
            progressBar.Visible = false;
        }

        void p_Exited(object sender, EventArgs e)
        {
            reloadUI();
        }

        // Clique do botão para iniciar as operações
        private void btnStartOperationClick(object sender, EventArgs e)
        {
            progressBar.Visible = true;
            btnStartOperation.Visible = false;

            // Iniciar os serviços de Apache e MySQL
            startWampApache();
            startWampMysql();

            // Caso seja primeira configuração
            if (!PoppedUtil.DBExists())
            {
                // Fixar o IP
                CreateDatabase();

                // Configurar projetos API e Dashboard
                 SetupGitProject("api");
                // Fixar IP e startar o projeto
                FixIP();

            } else if (!PoppedUtil.ApiExists())
            {
                // Configurar projetos API e Dashboard
                SetupGitProject("api");

                progressBar.Visible = false;
                lblMessage.Visible = true;
                gbIP.Visible = true;

                string localIp = PoppedUtil.GetLocalIPAddress();

                // Popular a label com IP local
                lblIP.Text = localIp + ":8080";

                // Livrar processos da porta 8000
                CleanProcess();

                // Fixar IP e startar o projeto
                FixIP();
            } else {

                progressBar.Visible = false;
                lblMessage.Visible = true;
                gbIP.Visible = true;

                string localIp = PoppedUtil.GetLocalIPAddress();

                // Popular a label com IP local
                lblIP.Text = localIp + ":8080";

                // Livrar processos da porta 8000
                CleanProcess();

                // FIxar o IP
                PoppedUtil.setIP(localIp);

                // Iniciar o laravel
                Task.Delay(10000).ContinueWith(t =>
                {
                    startProject(localIp);
                });

            }

        }

        private void startProject(string localIp)
        {
            ExecuteCommand("C:\\wamp\\bin\\php\\php7.0.29\\php.exe -S " + localIp + ":8080 -t \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\popped_local\\public\"", false, false);
        }

        // Iniciar o apache do WAMP
        private void startWampApache()
        {
            progressBar.Value = 50;
            ExecuteCommand("NET START wampapache");
        }
       
        // Iniciar o MySQL do WAMP
        private void startWampMysql()
        {
            progressBar.Value = 100;
            ExecuteCommand("NET START wampmysqld");
        }

        // Método para executar um comando no terminal
        public void ExecuteCommand(string Command, Boolean runAsAdmin = true, Boolean waitForExit = true, Boolean showCmd = false)
        {
            Console.WriteLine("COMANDO A SER EXECUTADO : " + Command);

            try {
                Process process = new System.Diagnostics.Process();

                ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = (showCmd ? ProcessWindowStyle.Normal : ProcessWindowStyle.Hidden),
                    FileName = "cmd.exe",
                    Arguments = "/C " + Command
                };

                if (!runAsAdmin)
                {
                    startInfo.Verb = "runas";
                }

                process.StartInfo = startInfo;
                process.Start();

               if(waitForExit)
                {
                    process.WaitForExit();
                }
            }
            catch (Exception e) {
                Console.WriteLine("Exception Execute Command : " + e.Message);
            }
        }

        // Criar o banco de dados a partir do dump nomeado de 'popped_local'
        public void CreateDatabase()
        {
            // Alterar senha do MySQL
            UpdateMySQLPassword();

            Thread.Sleep(2000);
            // Criar banco de dados
            ExecuteCommand("C:\\wamp\\bin\\mysql\\mysql5.7.21\\bin\\mysql -uroot --password=" + ConfigurationManager.AppSettings["db_password"] + " -e \"create schema if not exists local_popped\"");
            ExecuteCommand("C:\\wamp\\bin\\mysql\\mysql5.7.21\\bin\\mysql -uroot --password=" + ConfigurationManager.AppSettings["db_password"] + " local_popped < \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\popped_local.sql\"");
        }

        public void UpdateMySQLPassword()
        {
            string dbPassword = ConfigurationManager.AppSettings["db_password"];
            ExecuteCommand("C:\\wamp\\bin\\mysql\\mysql5.7.21\\bin\\mysql -uroot -e \"use mysql;update user set authentication_string=PASSWORD('" + dbPassword + "') where User='root';flush privileges;\"");  
        }

        private void FixIP()
        {
            progressBar.Visible = false;
            lblMessage.Visible = true;
            gbIP.Visible = true;

            string localIp = PoppedUtil.GetLocalIPAddress();

            // FIxar o IP
            PoppedUtil.setIP(localIp);
            // Popular a label com IP local
            lblIP.Text = localIp + ":8080";

            // Livrar processos da porta 8000
            CleanProcess();

            // Iniciar o laravel
            Task.Delay(10000).ContinueWith(t =>
            { 
                startProject(localIp);
            });

        }

        private void SetupGitProject(String project)
        {
            // Remover qualquer pasta que tenha o mesmo nome do projeto
            ExecuteCommand("rmdir " + ConfigurationManager.AppSettings[project + "_repository_folder"]);

            // Caminho do git
            String gitPath = ConfigurationManager.AppSettings["git_path"];

            // Desabilitar SSL git
            ExecuteCommand("\"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + gitPath + "\" config --system http.sslverify false");

            // Clonar o projeto
            ExecuteCommand("\"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + gitPath + "\" clone https://" +
                // Usuario
                ConfigurationManager.AppSettings[project + "_repository_user"] + ":" + 
                // Senha
                ConfigurationManager.AppSettings[project + "_repository_password"] + "@" +
                // URL sem https e @, exemplo:
                //bitbucket.com/projetopopped/api
                ConfigurationManager.AppSettings[project + "_repository_url"]);

            // Navegar até a pasta do projeto
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" && \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +"\\" + gitPath +  "\" checkout " + ConfigurationManager.AppSettings[project + "_repository_branch"] + "\"");
           
            // Atualizar o código
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" &&  \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + gitPath + "\" pull" + "\"");

            // Atualizar dependencias com o composer
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" && " + "C:\\wamp\\bin\\php\\php7.0.29\\php.exe \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\composer.phar\" update" + "\"", true, true, true);

            // Criar link para imagens
            ExecuteCommand("C:\\wamp\\bin\\php\\php7.0.29\\php.exe \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\popped_local\\artisan\" storage:link");
        }

        private void btnUpdateAPI_Click(object sender, EventArgs e)
        {
            // Atualizar projeto da API
            UpdateGitProject("api");
 
            MessageBox.Show("Ambiente atualizado!", "Sucesso :)");
        }

        // Método que realiza o update nos projetos utilizando como base o git configurado no arquivo App.config
        private void UpdateGitProject(String project)
        {
            // Caminho do git
            String gitPath = ConfigurationManager.AppSettings["git_path"];

            // Navegar até a pasta do projeto
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" &&  \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + gitPath + "\" checkout " + ConfigurationManager.AppSettings[project + "_repository_branch"] + "\"");

            // Atualizar o código
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" &&  \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + gitPath + "\" pull" + "\"");

            // Atualizar dependencias com o composer
            ExecuteCommand("cmd /c \"cd /d \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ConfigurationManager.AppSettings[project + "_repository_folder"] + "\" && " + "C:\\wamp\\bin\\php\\php7.0.29\\php.exe \"" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\composer.phar\" update" + "\"", true, true, true);
        }

        MemoryStream userInput = new MemoryStream();

        // Importar banco de dados e salvar na raiz do executavel
        private void btnImportDb_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "SQL files | *.sql"; // file types, that will be allowed to upload
                dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
                if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
                {
                    String path = dialog.FileName; // get name of file

                    String currentDirectory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                    File.Copy(path, Path.Combine(currentDirectory, Path.GetFileName(path)), true);

                    reloadUI();
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // Limpar task
        public void CleanProcess()
        {
            var prc = new ProcManager();
            prc.KillByPort(8080);
        }
    }
}

public class PRC
{
    public int PID { get; set; }
    public int Port { get; set; }
    public string Protocol { get; set; }
}
public class ProcManager
{
    public void KillByPort(int port)
    {
        var processes = GetAllProcesses();
        if (processes.Any(p => p.Port == port))
            try
            {
                Process.GetProcessById(processes.First(p => p.Port == port).PID).Kill();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        else
        {
            Console.WriteLine("No process to kill!");
        }
    }

    public List<PRC> GetAllProcesses()
    {
        var pStartInfo = new ProcessStartInfo();
        pStartInfo.FileName = "netstat.exe";
        pStartInfo.Arguments = "-a -n -o";
        pStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
        pStartInfo.UseShellExecute = false;
        pStartInfo.RedirectStandardInput = true;
        pStartInfo.RedirectStandardOutput = true;
        pStartInfo.RedirectStandardError = true;

        var process = new Process()
        {
            StartInfo = pStartInfo
        };
        process.Start();

        var soStream = process.StandardOutput;

        var output = soStream.ReadToEnd();
        if (process.ExitCode != 0)
            throw new Exception("somethign broke");

        var result = new List<PRC>();

        var lines = Regex.Split(output, "\r\n");
        foreach (var line in lines)
        {
            if (line.Trim().StartsWith("Proto"))
                continue;

            var parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var len = parts.Length;
            if (len > 2)
                result.Add(new PRC
                {
                    Protocol = parts[0],
                    Port = int.Parse(parts[1].Split(':').Last()),
                    PID = int.Parse(parts[len - 1])
                });


        }
        return result;
    }
}