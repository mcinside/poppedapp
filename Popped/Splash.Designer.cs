﻿namespace Popped
{
    partial class Splash
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.btnInitialConfig = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.btnStartOperation = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblMessage = new System.Windows.Forms.Label();
            this.gbIP = new System.Windows.Forms.GroupBox();
            this.lblIP = new System.Windows.Forms.Label();
            this.lblFinishing = new System.Windows.Forms.Label();
            this.btnUpdateAPI = new System.Windows.Forms.Button();
            this.btnImportDb = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.gbIP.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnInitialConfig
            // 
            this.btnInitialConfig.BackColor = System.Drawing.Color.Transparent;
            this.btnInitialConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInitialConfig.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInitialConfig.ForeColor = System.Drawing.Color.Turquoise;
            this.btnInitialConfig.Location = new System.Drawing.Point(177, 182);
            this.btnInitialConfig.Margin = new System.Windows.Forms.Padding(2);
            this.btnInitialConfig.Name = "btnInitialConfig";
            this.btnInitialConfig.Size = new System.Drawing.Size(155, 52);
            this.btnInitialConfig.TabIndex = 0;
            this.btnInitialConfig.Text = "Iniciar Configuração";
            this.btnInitialConfig.UseVisualStyleBackColor = false;
            this.btnInitialConfig.Click += new System.EventHandler(this.btnInitialConfigClick);
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.Location = new System.Drawing.Point(152, 60);
            this.pbLogo.Margin = new System.Windows.Forms.Padding(2);
            this.pbLogo.MaximumSize = new System.Drawing.Size(200, 65);
            this.pbLogo.MinimumSize = new System.Drawing.Size(200, 65);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(200, 65);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 1;
            this.pbLogo.TabStop = false;
            // 
            // btnStartOperation
            // 
            this.btnStartOperation.BackColor = System.Drawing.Color.Transparent;
            this.btnStartOperation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartOperation.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartOperation.ForeColor = System.Drawing.Color.Turquoise;
            this.btnStartOperation.Location = new System.Drawing.Point(177, 182);
            this.btnStartOperation.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartOperation.Name = "btnStartOperation";
            this.btnStartOperation.Size = new System.Drawing.Size(155, 52);
            this.btnStartOperation.TabIndex = 2;
            this.btnStartOperation.Text = "Iniciar Operação";
            this.btnStartOperation.UseVisualStyleBackColor = false;
            this.btnStartOperation.Click += new System.EventHandler(this.btnStartOperationClick);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(152, 146);
            this.progressBar.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(200, 15);
            this.progressBar.TabIndex = 3;
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.OliveDrab;
            this.lblMessage.Location = new System.Drawing.Point(98, 159);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(326, 76);
            this.lblMessage.TabIndex = 4;
            this.lblMessage.Text = "O ambiente está em operação! Mantenha essa tela aberta ou minimizada.";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMessage.Visible = false;
            // 
            // gbIP
            // 
            this.gbIP.Controls.Add(this.lblIP);
            this.gbIP.Location = new System.Drawing.Point(104, 254);
            this.gbIP.Margin = new System.Windows.Forms.Padding(2);
            this.gbIP.Name = "gbIP";
            this.gbIP.Padding = new System.Windows.Forms.Padding(2);
            this.gbIP.Size = new System.Drawing.Size(301, 74);
            this.gbIP.TabIndex = 5;
            this.gbIP.TabStop = false;
            this.gbIP.Text = "IP PARA USO NO APP";
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.lblIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIP.Location = new System.Drawing.Point(42, 25);
            this.lblIP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(98, 31);
            this.lblIP.TabIndex = 6;
            this.lblIP.Text = "0.0.0.0";
            // 
            // lblFinishing
            // 
            this.lblFinishing.AutoSize = true;
            this.lblFinishing.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinishing.Location = new System.Drawing.Point(133, 159);
            this.lblFinishing.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFinishing.Name = "lblFinishing";
            this.lblFinishing.Size = new System.Drawing.Size(256, 29);
            this.lblFinishing.TabIndex = 6;
            this.lblFinishing.Text = "Finalizando, aguarde...";
            this.lblFinishing.Visible = false;
            // 
            // btnUpdateAPI
            // 
            this.btnUpdateAPI.BackColor = System.Drawing.Color.White;
            this.btnUpdateAPI.FlatAppearance.BorderSize = 0;
            this.btnUpdateAPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateAPI.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnUpdateAPI.Location = new System.Drawing.Point(394, 12);
            this.btnUpdateAPI.Margin = new System.Windows.Forms.Padding(0);
            this.btnUpdateAPI.Name = "btnUpdateAPI";
            this.btnUpdateAPI.Size = new System.Drawing.Size(117, 28);
            this.btnUpdateAPI.TabIndex = 8;
            this.btnUpdateAPI.Text = "Atualizar Versão";
            this.btnUpdateAPI.UseVisualStyleBackColor = false;
            this.btnUpdateAPI.Visible = false;
            this.btnUpdateAPI.Click += new System.EventHandler(this.btnUpdateAPI_Click);
            // 
            // btnImportDb
            // 
            this.btnImportDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportDb.Location = new System.Drawing.Point(177, 182);
            this.btnImportDb.Margin = new System.Windows.Forms.Padding(0);
            this.btnImportDb.Name = "btnImportDb";
            this.btnImportDb.Size = new System.Drawing.Size(148, 23);
            this.btnImportDb.TabIndex = 9;
            this.btnImportDb.Text = "Importar Banco de Dados";
            this.btnImportDb.UseVisualStyleBackColor = true;
            this.btnImportDb.Visible = false;
            this.btnImportDb.Click += new System.EventHandler(this.btnImportDb_Click);
            // 
            // Splash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(523, 366);
            this.Controls.Add(this.btnImportDb);
            this.Controls.Add(this.btnUpdateAPI);
            this.Controls.Add(this.lblFinishing);
            this.Controls.Add(this.gbIP);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnStartOperation);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.btnInitialConfig);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(539, 405);
            this.MinimumSize = new System.Drawing.Size(539, 405);
            this.Name = "Splash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Popped - Restaurante";
            this.Load += new System.EventHandler(this.Splash_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.gbIP.ResumeLayout(false);
            this.gbIP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInitialConfig;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Button btnStartOperation;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.GroupBox gbIP;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Label lblFinishing;
        private System.Windows.Forms.Button btnUpdateAPI;
        private System.Windows.Forms.Button btnImportDb;
    }
}

