
#include <idp.iss>

#define MyAppName "Popped"
#define MyAppVersion "1.0.0"
#define MyAppPublisher ""
#define MyAppURL ""
#define MyAppExeName "Popped.exe"

[Setup]
; Configura��es do Instalador
; Para gerar um novo GUID, clique em Tools | Generate GUID
AppId={{B4524FC0-5D08-4ED9-BB5C-F51D72B670A7}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={sd}\PoppedApp
DisableDirPage=true
DefaultGroupName={#MyAppPublisher}
OutputBaseFilename=Popped
Password=
Compression=lzma/ultra64
SolidCompression=true
WizardImageBackColor=clBlack
WindowVisible=false
PrivilegesRequired=admin
DisableProgramGroupPage=true
ShowTasksTreeLines=false
InternalCompressLevel=ultra64
DisableStartupPrompt=true
AlwaysShowComponentsList=false
FlatComponentsList=false
ShowComponentSizes=false
UsePreviousSetupType=true
UsePreviousTasks=false
TerminalServicesAware=false

[Languages]
Name: brazilianportuguese; MessagesFile: compiler:Languages\BrazilianPortuguese.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: dontinheritcheck; Components: Popped

[Files]
; Inclua todos os arquivos que s�o necess�rios para a instala��o do app (incluindo dll's)
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\composer.phar; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Google.Protobuf.dll; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Google.Protobuf.xml; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\my-config.ini; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\MySql.Data.dll; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\MySql.Data.xml; DestDir: {app}
; Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\popped_local.sql; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\wamp.exe; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Popped.exe; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Popped.exe.config; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Popped.pdb; DestDir: {app}
Source: C:\Users\mauri\Documents\Projetos\PoppedApp\Popped\bin\Release\Git\*; DestDir: {app}\Git; Flags: ignoreversion recursesubdirs


[Icons]
; Caminho para cria��o do icone (default no Desktop)
Name: {group}\Popped; Filename: {app}\Popped.exe; Languages: ; Tasks: desktopicon; Components: Popped
Name: {commondesktop}\Popped; Filename: {app}\Popped.exe; Tasks: desktopicon; Components: Popped

[Run]
Filename: {app}\Popped.exe; Description: {cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}; Flags: runascurrentuser nowait postinstall skipifsilent; Tasks: ; Languages: ; Components: Popped

[Components]
Name: Popped; Description: Popped; Languages: ; Types: Instalacao_Popped
[_ISTool]
UseAbsolutePaths=false
[Types]
Name: Instalacao_Popped; Description: Instala��o Popped; Languages: 

; Instala��o do .NET Framework por Default
[Code]
function Framework45IsNotInstalled(): Boolean;
var
  bSuccess: Boolean;
  regVersion: Cardinal;
begin
  Result := True;

  bSuccess := RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Release', regVersion);
  if (True = bSuccess) and (regVersion >= 378389) then begin
    Result := False;
  end;
end;

procedure InitializeWizard;
begin
  if Framework45IsNotInstalled() then
  begin
    idpAddFile('http://go.microsoft.com/fwlink/?LinkId=397707', ExpandConstant('{tmp}\NetFrameworkInstaller.exe'));
    idpDownloadAfter(wpReady);
  end;
end;

procedure InstallFramework;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing .NET Framework 4.5.2. This might take a few minutes...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not Exec(ExpandConstant('{tmp}\NetFrameworkInstaller.exe'), '/passive /norestart', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
    begin
      MsgBox('.NET installation failed with code: ' + IntToStr(ResultCode) + '.', mbError, MB_OK);
    end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;

    DeleteFile(ExpandConstant('{tmp}\NetFrameworkInstaller.exe'));
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  case CurStep of
    ssPostInstall:
      begin
        if Framework45IsNotInstalled() then
        begin
          InstallFramework();
        end;
      end;
  end;
end;
